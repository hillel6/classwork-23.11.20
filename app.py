from flask import Flask, render_template, request, make_response, session, redirect, url_for
import sqlite3

# Инициализация приложения Flask
app = Flask(__name__)


"""
Созданная таблица для этого примера
                                             varchar это тип данных похож на text только разница в том что
                                             текст занимает около расчитан на большие обьемы текста и занимает
                                             много места в базе данных, но если вы знаете что данные в столбике будут
                                             не особо большие например Имя, то можно использовать varchar 
                                             с указанием максимального размера столбика, в данном примере 200
                                                  |
CREATE TABLE students (id int primary key, name varchar(200), surname varchar(200), age int);

"""


@app.route('/students')
def show_students():
    # Подключение к базе данных
    connection = sqlite3.connect('database.sqlite')
    # Инициализация курсора для выполнения операций
    cursor = connection.cursor()
    # в методе execute можно вставлять любые SQL команды
    cursor.execute("SELECT * FROM students")
    # в случае если надо передать аргументы в строку
    # лучше это делать так, передавать аргумент
    # или список аргументов вторым параметром
    # пример:
    """
      если один параметр
      >>> cursor.execute("SELECT * FROM students WHERE id < ?", 10)
      если много параметров
      >>> cursor.execute("SELECT * FROM students WHERE id < ? AND id > ?", (10, 5))
    """

    # Далее чтобы забрать результат команды SELECT используем метод fetchall
    students = cursor.fetchall()

    # Обязательно закрываем соединение
    connection.close()

    # Передаем список студентов в темплейт
    return render_template('index.html', students=students)


@app.route('/students/add')
def add_student():
    # Получаем из аргументов информацию по студенту
    # для передачи несколько аргументов в строке браузере используют разделитель &, пример
    # localhost:5000/students/add?name=Name&surname=Surname&age=10
    student_name = request.args.get('name')
    student_surname = request.args.get('surname')
    student_age = request.args.get('age')

    # В случае если небыл введено имя выкидываем ошибку
    if not student_name:
        return 'Sorry, you should insert student name'

    # Подключение к базе данных
    connection = sqlite3.connect('database.sqlite')
    # Инициализация курсора для выполнения операций
    cursor = connection.cursor()

    # Далее располагаем данные в том порядке в котором хотим записать в базу данных
    values = (student_name, student_surname, student_age)
    #                                             (student_name, student_surname, student_age)
    #                                                 |              |               |
    cursor.execute(
        """INSERT INTO students (id,                 name,         surname,         age) 
           VALUES               (last_insert_rowid(),  ?,             ?,             ? )""",
        values
    )
    # Что делать с id в случае если при создании таблицы небыло указанно autoincrement
    # То при добавлении новой строчки можно использовать готовую встроенную функцию в sqlite,
    # last_insert_rowid() она сама узнает какой последний номер id был в таблице и сама
    # вставит новый id на единицу больше

    # но лучше все таки указывать автоинкремент на этапе создании таблицы, тогда не надо будет
    # прописывать эту функцию (last_insert_rowid()),
    # пример создание таблицы с автоинкрементом
    # CREATE TABLE students (id int primary key autoincrement, name varchar(200), surname varchar(200), age int);

    # В случаях когда мы создаем таблицу, добавляем запись, обновляем запись, удаляем запись
    # и все остальные операции которые требуют исполнения их нужно отправлять в базу данных на исполнение
    # для этого используем метод commit() в connection
    connection.commit()

    # Обязательно закрываем соединение
    connection.close()

    # Поскольку мы успешно создали юзера можем сделать редирект(перенаправление) на страницу со всеми студентами
    # для этого используем функцию redirect
    # Это даст понять браузеру что надо открыть страницу /students
    return redirect('/students')


"""
Лучше запуск сервера app.run(debug=True)
вставлять в условие if __name__ == '__main__':
это нужно для того чтобы этот файл не запустил сервер
если заимпортить этот файл из другого питоновского файла 
"""
if __name__ == '__main__':
    app.run(debug=True)
